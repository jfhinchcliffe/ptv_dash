require 'nokogiri'
require 'open-uri'

class StaticPagesController < ApplicationController
  def home
  end
  
  def dash
    #new method - loops through H2's, looks for the @search_for variable (h2 on the destination page) and assigns CSS selector based on position.
    page = Nokogiri::HTML(open('http://www.ptv.vic.gov.au/live-travel-updates'))
    
    @posts = Post.all
    
    @mode_headings = page.xpath("//div/a/h2").collect {|node| node.text.strip}
    @search_for = "Additional information for Regional trains" #this is the string that we're searching for. Can be changed.
    @css_counter = 0 #offset by 1 to get true CSS position on page (Ruby starts from 0 rather than 1)
    
    
    @mode_headings.each do |mh|
      if mh == @search_for
        @css_position = @css_counter + 1
        @css_counter += 1
      else
        @css_counter += 1
      end
    end
    #these 2 CSS selectors pinpoint the position of the @search_for value above so it can be scraped.
    @vline_current_disruption = page.css(".article-grey-gradient:nth-child(#{@css_position}) .subGroup:nth-child(1) .disruptionArticleLink")
    @vline_upcoming_disruption = page.css(".article-grey-gradient:nth-child(#{@css_position}) .subGroup+ .subGroup .disruptionArticleLink")
    
    #OLD METHODS BE HERE
    #new method - counts H2's, subtracts 3 (to get to location of 'Regional Bus')
    # and then uses this to populate the CSS finder so it won't get out of whack.
    #regional bus will always be the third from the end, so can't see this breaking
    
    #@disruptions = page.css('h2').collect {|node| node.text.strip}
    #@page_h2_elements = page.xpath("//div/a/h2").collect {|node| node.text.strip}
    #@total_elements_count = @page_h2_elements.count
    #@regional_trains_location = @total_elements_count - 2
    #@vline_current_disruption = page.css(".article-grey-gradient:nth-child(#{@regional_trains_location}) .subGroup:nth-child(1) .disruptionArticleLink")
    #@vline_upcoming_disruption = page.css(".article-grey-gradient:nth-child(#{@regional_trains_location}) .subGroup+ .subGroup .disruptionArticleLink")
    
    #OLD METHOD FOR FINDING THE REGIONAL TRAINS CSS DIV NUMBERS
    
    #putting all variables in Static_Pages controller.
    #page = Nokogiri::HTML(open('http://www.ptv.vic.gov.au/live-travel-updates'))
    #Find all the H2 divs and strip the text out
    #@page_h2_elements = page.xpath("//div/a/h2").collect {|node| node.text.strip}
    #count the items in test_current to get the location of Regional Trains (starts from 0)
    #@regional_trains_location = @page_h2_elements.index("Additional information for Regional trains")
  
    #The CSS pointer changes if there's a 'General' alert on the site.
    #this code goes through the CSS, finds the 'Regional Trains' selector
    #and sets the CSS correctly so it doesn't get out of whack - the index
    #count starts from 0, so needs to be +1'd so it always finds the correct CSS

    
    # add 1 so that the CSS location matches up with the .index count 
    #@css_counter = @regional_trains_location + 1
    #change CSS selector with the css_counter injected in there so it's always accurate. 
    #@vline_current_disruption = page.css(".article-grey-gradient:nth-child(#{@css_counter}) .subGroup:nth-child(1) .disruptionArticleLink")
    #@vline_upcoming_disruption = page.css(".article-grey-gradient:nth-child(#{@css_counter}) .subGroup+ .subGroup .disruptionArticleLink")
  end
  
  def dashtwo
    #test dash
    #MAYBE if page contains general??@! we can then set the CSS selectore for upcoming / current disruptions accordingly.
    #testing various ways to get the best selector 
    page = Nokogiri::HTML(open('http://www.ptv.vic.gov.au/live-travel-updates'))
    #@details = @page.css('details').find{|node| node.css('h2').class == "articles-header Regional trains"}
    #@disruptions = page.css('h2/ul/li').collect {|node| node.text.strip}
    
    @disruptions = page.css('h2').collect {|node| node.text.strip}
    @page_h2_elements = page.xpath("//div/a/h2").collect {|node| node.text.strip}
    @total_elements_count = @page_h2_elements.count
    @regional_trains_location = @total_elements_count - 3
    @vline_current_disruption = page.css(".article-grey-gradient:nth-child(#{@regional_trains_location}) .subGroup:nth-child(1) .disruptionArticleLink")
    @vline_upcoming_disruption = page.css(".article-grey-gradient:nth-child(#{@regional_trains_location}) .subGroup+ .subGroup .disruptionArticleLink")
    
    
    
    #@disruptions = p page.xpath('//*[preceding-sibling::a][following-sibling::c]')
      
     # English: Starting at the root of the document: look in every div that has a class name containing the word 'product'. Inside that find a link. In that link find h4 text.

    #  XPath: //div[contains(@class,'product')]/a/h4
    
  end
  
end
